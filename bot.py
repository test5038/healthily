import os
import requests
import json
from openai import OpenAI
from dotenv import load_dotenv

load_dotenv()

client = OpenAI(
    api_key=os.getenv("API_KEY")
)

def query_gpt(messages):
    """
    This function sends a prompt to the GPT API and returns the response.
    """
    try:
        response = client.chat.completions.create(
            model="gpt-3.5-turbo-0125",
            messages=messages,
            temperature=0.7,
            max_tokens=150,
            top_p=1.0,
            frequency_penalty=0.0,
            presence_penalty=0.0
        )
        return response.choices[0].message.content.strip()
    except Exception as e:
        print(f"An error occurred: {e}")
        return "I'm sorry, I couldn't process that request."

def main():
    print("Welcome to Healthily Chat! Type 'exit' to quit.")

    healthily_url = 'https://portal.your.md/v4/chat'
    healthily_headers = {
        'x-api-key': os.getenv("HEALTHILY_API_KEY"),
        'Authorization': 'Bearer ' + os.getenv("HEALTHLITY_AUTHORISATION"),
        'Content-Type': 'application/json',
    }
    healthily_data = {
    }
    healthily_response = requests.post(healthily_url, json=healthily_data, headers=healthily_headers)
    healthily_response_data = {}
    if healthily_response.status_code == 200:
        healthily_response_data = healthily_response.json()
        print("Success with conversation id: ", healthily_response_data['conversation']['id'])
        # print(healthily_response_data)
    else:
        print(f"Error: {healthily_response.status_code} - {healthily_response.text}")

    all_messages = '';
    while True:
        if healthily_response_data['question']['type'] == "name":
            print(f"Healthily: Could I have your {healthily_response_data['question']['type']}?")
        user_input = input("You: ")
        if user_input.lower() == 'exit':
            break
        
        all_messages += f"you: {user_input}\n"
        all_messages += f"Healthily: {json.dumps(healthily_response_data)}\n"

        if healthily_response_data['question']['type'] == "name":
            healthily_data = {"answer":{"type":"name","value":user_input},"conversation":{"id":healthily_response_data['conversation']['id']}}
            healthily_response = requests.post(healthily_url, json=healthily_data, headers=healthily_headers)
            if healthily_response.status_code == 200:
                healthily_response_data = healthily_response.json()
                # print(healthily_response_data)
            else:
                # print(f"Error: {healthily_response.status_code} - {healthily_response.text}")
                print ('---------------------------------')
        elif healthily_response_data['question']['type'] == "sex":
            healthily_data = {"answer":{"type":"sex","selection":[user_input]},"conversation":{"id":healthily_response_data['conversation']['id']}}
            healthily_response = requests.post(healthily_url, json=healthily_data, headers=healthily_headers)
            if healthily_response.status_code == 200:
                healthily_response_data = healthily_response.json()
                # print(healthily_response_data)
            else:
                # print(f"Error: {healthily_response.status_code} - {healthily_response.text}")
                print ('---------------------------------')
        elif healthily_response_data['question']['type'] == "year_of_birth":
            healthily_data = {"answer":{"type":"year_of_birth","value":str(user_input)},"conversation":{"id":healthily_response_data['conversation']['id']}}
            healthily_response = requests.post(healthily_url, json=healthily_data, headers=healthily_headers)
            if healthily_response.status_code == 200:
                healthily_response_data = healthily_response.json()
                # print(healthily_response_data)
            else:
                # print(f"Error: {healthily_response.status_code} - {healthily_response.text}")
                print ('---------------------------------')
        elif healthily_response_data['question']['type'] == "initial_symptom":
            healthily_data = {"answer":{"type":"initial_symptom","value":user_input},"conversation":{"id":healthily_response_data['conversation']['id']}}
            healthily_response = requests.post(healthily_url, json=healthily_data, headers=healthily_headers)
            if healthily_response.status_code == 200:
                healthily_response_data = healthily_response.json()
                # print(healthily_response_data)
            else:
                # print(f"Error: {healthily_response.status_code} - {healthily_response.text}")
                print ('---------------------------------')
        elif healthily_response_data['question']['type'] == "generic":
            healthily_data = {"answer":{"type":"generic","input":{"exclude":["information"],"include":["assessment"]}},"conversation":{"id":healthily_response_data['conversation']['id']}}
            healthily_response = requests.post(healthily_url, json=healthily_data, headers=healthily_headers)
            if healthily_response.status_code == 200:
                healthily_response_data = healthily_response.json()
                # print(healthily_response_data)
            else:
                # print(f"Error: {healthily_response.status_code} - {healthily_response.text}")
                print ('---------------------------------')
            

        all_messages += f"Healthily: {json.dumps(healthily_response_data)}\n"

        response = query_gpt([
            {"role": "system", "content":  all_messages + ":analyse the json, based on the json ask the patient question."},
            {"role": "system", "content": "you a are a healthily gp assistant, if a patient answered the questions, list out the answers. and ask next question based on  the json"},
            {"role": "user", "content": user_input}
        ])
        
        print(f"Healthily: {response}")

if __name__ == "__main__":
    main()
