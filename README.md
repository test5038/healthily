This is a demo POC of how ai bot connected to healthily symptoms assessment.

make sure you got .env in the project with the following keys:

```bash
API_KEY=<CHATGPT API KEY>
HEALTHILY_API_KEY=<HEALTHILY_API_KEY>
HEALTHILY_API_TOKEN=<HEALTHLY_TOKEN>
HEALTHLITY_AUTHORISATION=<LOGIN_AUTHORISATION>
```

To get HEALTHLITY_AUTHORISATION, a simple way is to run the sample and fetch from the API request:
https://github.com/YourMD/healthily-symptom-checker-sample


